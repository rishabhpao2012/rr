1
00:01:01,561 --> 00:01:04,564
<font face="Serif" size="18">ALL CHARACTERS AND EVENTS
DEPICTED IN THIS DRAMA ARE FICTITIOUS</font>

2
00:01:05,815 --> 00:01:07,692
<font face="Serif" size="18">EPISODE 5</font>

3
00:01:50,693 --> 00:01:53,238
<font face="Serif" size="18"><i><font size="18"><font face="Serif">We're covering
the dorm's front door. Over.</font></font></i></font>

4
00:01:54,739 --> 00:01:57,200
<font face="Serif" size="18"><i><font size="18"><font face="Serif">We're approaching too as I speak. Over.</font></font></i></font>

5
00:02:36,865 --> 00:02:37,991
<font face="Serif" size="18">Lim Soo-ho!</font>

6
00:02:43,913 --> 00:02:45,123
<font face="Serif" size="18">Drop the guns!</font>

7
00:02:46,124 --> 00:02:47,292
<font face="Serif" size="18">Lim Soo-ho.</font>

8
00:02:48,251 --> 00:02:49,711
<font face="Serif" size="18">You're surrounded.</font>

9
00:02:52,172 --> 00:02:53,298
<font face="Serif" size="18">Stay away.</font>

10
00:02:53,882 --> 00:02:55,341
<font face="Serif" size="18">I'll shoot her.</font>

11
00:02:55,425 --> 00:02:56,259
<font face="Serif" size="18">Come down.</font>

12
00:03:00,430 --> 00:03:01,848
<font face="Serif" size="18">Stay away!</font>

13
00:03:01,931 --> 00:03:02,974
<font face="Serif" size="18">Stay away!</font>

14
00:03:03,850 --> 00:03:05,185
<font face="Serif" size="18">The students will get hurt.</font>

15
00:03:10,481 --> 00:03:12,775
<font face="Serif" size="18">Hey, it's all right.</font>

16
00:03:13,359 --> 00:03:14,194
<font face="Serif" size="18">Don't be scared.</font>

17
00:03:15,028 --> 00:03:16,571
<font face="Serif" size="18">It's all right.</font>

18
00:03:25,496 --> 00:03:26,623
<font face="Serif" size="18">Drop the guns.</font>

19
00:03:32,420 --> 00:03:34,130
<font face="Serif" size="18">Do it in three seconds, or she's dead.</font>

20
00:03:37,258 --> 00:03:38,509
<font face="Serif" size="18">One.</font>

21
00:03:42,597 --> 00:03:43,598
<font face="Serif" size="18">Two.</font>

22
00:03:50,271 --> 00:03:51,272
<font face="Serif" size="18">Lim Soo-ho.</font>

23
00:04:04,994 --> 00:04:05,870
<font face="Serif" size="18">Three.</font>

24
00:04:12,669 --> 00:04:13,795
<font face="Serif" size="18">Comrade!</font>

25
00:04:44,450 --> 00:04:45,451
<font face="Serif" size="18"><i><font size="18"><font face="Serif">Detective Jang.</font></font></i></font>

26
00:04:45,535 --> 00:04:47,453
<font face="Serif" size="18">Who shoots during a civil defense drill?</font>

27
00:04:48,162 --> 00:04:49,038
<font face="Serif" size="18">Don't tell me</font>

28
00:04:49,122 --> 00:04:50,999
<font face="Serif" size="18">war's broken out.</font>

