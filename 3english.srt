1
00:01:01,561 --> 00:01:04,564
<font face="Serif" size="18">ALL CHARACTERS AND EVENTS
DEPICTED IN THIS DRAMA ARE FICTITIOUS</font>

2
00:01:07,734 --> 00:01:09,444
<font face="Serif" size="18">EPISODE 3</font>

3
00:01:15,575 --> 00:01:20,163
<font face="Serif" size="18">Why would you want to check
this place this late at night?</font>

4
00:01:22,123 --> 00:01:24,125
<font face="Serif" size="18">God, it's so dusty here.</font>

5
00:01:32,342 --> 00:01:33,551
<font face="Serif" size="18">Oh, no.</font>

6
00:01:38,014 --> 00:01:38,848
<font face="Serif" size="18">Open it.</font>

7
00:01:39,641 --> 00:01:41,935
<font face="Serif" size="18">Yes, ma'am. I'm looking for the key.</font>

8
00:01:42,185 --> 00:01:44,562
<font face="Serif" size="18">Which one is it? There are so many.</font>

9
00:01:44,646 --> 00:01:45,855
<font face="Serif" size="18">Now.</font>

10
00:01:46,689 --> 00:01:48,358
<font face="Serif" size="18">Okay, I found it.</font>

11
00:01:54,823 --> 00:01:56,908
<font face="Serif" size="18">The door's quite stiff.</font>

12
00:02:15,885 --> 00:02:16,886
<font face="Serif" size="18">Turn the lights on.</font>

13
00:02:16,970 --> 00:02:20,974
<font face="Serif" size="18">Yes, ma'am. Where's the switch?</font>

14
00:02:21,349 --> 00:02:22,767
<font face="Serif" size="18">There it is.</font>

15
00:02:30,984 --> 00:02:35,363
<font face="Serif" size="18">My, what do we do?
The lights are not working.</font>

16
00:02:36,489 --> 00:02:37,740
<font face="Serif" size="18">They're not working?</font>

17
00:02:37,824 --> 00:02:39,993
<font face="Serif" size="18">The room hasn't been used for six years,</font>

18
00:02:40,076 --> 00:02:41,911
<font face="Serif" size="18">so I didn't know.</font>

19
00:02:52,380 --> 00:02:54,799
<font face="Serif" size="18">KO HYE-RYEONG</font>

20
00:03:37,342 --> 00:03:38,843
<font face="Serif" size="18">Why did you come here?</font>

21
00:03:38,927 --> 00:03:40,220
<font face="Serif" size="18">Just followed her.</font>

22
00:03:40,303 --> 00:03:41,679
<font face="Serif" size="18">You should be here.</font>

23
00:03:44,098 --> 00:03:45,892
<font face="Serif" size="18">There's water on the bathroom floor.</font>

24
00:03:46,476 --> 00:03:48,353
<font face="Serif" size="18">Someone must be staying here.</font>

25
00:03:48,436 --> 00:03:51,189
<font face="Serif" size="18">Well, the ceiling
must've leaked rainwater.</font>

26
00:03:51,272 --> 00:03:53,733
<font face="Serif" size="18">It didn't rain for days. What rainwater?</font>

27
00:03:55,526 --> 00:03:57,862
<font face="Serif" size="18">It must've been trapped in the ceiling.</font>

28
00:03:57,946 --> 00:04:00,406
<font face="Serif" size="18">-Hey.
-I'll come check tomorrow morning.</font>

29
00:04:00,490 --> 00:04:01,366
<font face="Serif" size="18">The window.</font>

30
00:04:01,449 --> 00:04:05,536
<font face="Serif" size="18">Gosh, this place is so spooky.
Could we go down now?</font>

31
00:04:06,746 --> 00:04:07,580
<font face="Serif" size="18">Ma'am.</font>

32
00:04:11,042 --> 00:04:13,711
<font face="Serif" size="18">I think there's something out there.</font>

33
00:04:34,315 --> 00:04:35,775
<font face="Serif" size="18">Don't look down.</font>

34
00:04:36,484 --> 00:04:37,777
<font face="Serif" size="18">Look at me instead.</font>

35
00:04:39,654 --> 00:04:40,989
<font face="Serif" size="18">Keep looking at me.</font>

