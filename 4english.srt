1
00:01:01,561 --> 00:01:04,564
<font face="Serif" size="18">ALL CHARACTERS AND EVENTS
DEPICTED IN THIS DRAMA ARE FICTITIOUS</font>

2
00:01:07,317 --> 00:01:08,985
<font face="Serif" size="18">EPISODE 4</font>

3
00:02:56,968 --> 00:02:58,720
<font face="Serif" size="18">But this is…</font>

4
00:03:00,054 --> 00:03:02,307
<font face="Serif" size="18">Your sister gave it to you.</font>

5
00:03:03,683 --> 00:03:06,019
<font face="Serif" size="18">I think I can only leave…</font>

6
00:03:09,105 --> 00:03:10,607
<font face="Serif" size="18">without worrying about you</font>

7
00:03:12,233 --> 00:03:13,860
<font face="Serif" size="18">when I know you're wearing it.</font>

8
00:03:27,749 --> 00:03:30,376
<font face="Serif" size="18">But you need to wear it to stay safe.</font>

9
00:03:30,960 --> 00:03:32,629
<font face="Serif" size="18">I'm not the one being chased.</font>

10
00:03:38,885 --> 00:03:40,094
<font face="Serif" size="18">Just keep it.</font>

11
00:03:42,513 --> 00:03:44,724
<font face="Serif" size="18">It's the only thing I can give you.</font>

12
00:03:54,150 --> 00:03:55,026
<font face="Serif" size="18">Lim Soo-ho…</font>

13
00:03:55,109 --> 00:03:56,486
<font face="Serif" size="18">What's taking them so long?</font>

14
00:03:57,487 --> 00:03:59,405
<font face="Serif" size="18">They're going to miss him.</font>

15
00:03:59,489 --> 00:04:00,657
<font face="Serif" size="18">Damn it.</font>

16
00:04:07,747 --> 00:04:11,751
<font face="Serif" size="18">Gwang-tae, don't tell me
you called the cops.</font>

17
00:04:12,210 --> 00:04:13,753
<font face="Serif" size="18">If you know who he really is--</font>

18
00:04:13,836 --> 00:04:17,048
<font face="Serif" size="18">If he gets caught, I'll be arrested too!</font>

19
00:04:20,802 --> 00:04:23,805
<font face="Serif" size="18">Why will you be arrested?
You have nothing to do with him.</font>

20
00:04:25,974 --> 00:04:27,725
<font face="Serif" size="18">My roommates</font>

21
00:04:28,518 --> 00:04:30,728
<font face="Serif" size="18">have been hiding him.</font>

22
00:04:31,896 --> 00:04:33,773
<font face="Serif" size="18">He came into our room,</font>

23
00:04:33,856 --> 00:04:35,900
<font face="Serif" size="18">so we've been hiding him.</font>

24
00:04:39,362 --> 00:04:40,196
<font face="Serif" size="18">Darn it.</font>

25
00:04:40,280 --> 00:04:41,656
<font face="Serif" size="18">You traitor!</font>

26
00:04:51,040 --> 00:04:53,418
<font face="Serif" size="18">Yeong-ro!</font>

27
00:04:55,753 --> 00:04:57,297
<font face="Serif" size="18">Gwang-tae saw him!</font>

28
00:04:59,507 --> 00:05:00,967
<font face="Serif" size="18">Hurry up. Go now!</font>

